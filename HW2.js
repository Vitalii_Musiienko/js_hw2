let userName, userAge;

do {
    userName = prompt('What is your name?');
} while (!userName);
do {
    userAge = prompt('How old are you?', [userAge]);
} while (!+userAge);

if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (18 <= userAge <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome, ' + userName + '!');
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert('Welcome, ' + userName + '!');
}